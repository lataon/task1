package spring.java.lab2021;

public final class Constants {
    private Constants(){}

    public  static  final String WRONG_SYMBOLS = "Expression has wrong symbols";
    public  static  final String WRONG_PARENTHESES_NUMBER = "Неверное выражение: Скобочки";
    public  static  final String ARITHMETIC_MISTAKE = "Неверное выражение: Арифметическая ошибка";
}
