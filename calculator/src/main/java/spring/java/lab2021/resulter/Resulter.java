package spring.java.lab2021.resulter;

import spring.java.lab2021.interfaces.Resultable;
import spring.java.lab2021.model.ExpressionCalculator;

import java.io.IOException;
import java.io.Writer;

public class Resulter implements Resultable {
    public void to(String line, Writer writer) throws IOException {
        writer.write(String.valueOf(ExpressionCalculator.calculate(line)));
    }
}
