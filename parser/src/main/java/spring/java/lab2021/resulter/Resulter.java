package spring.java.lab2021.resulter;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import spring.java.lab2021.interfaces.Resultable;
import spring.java.lab2021.model.Person;

import java.io.IOException;
import java.io.Writer;

public class Resulter implements Resultable {
    @Override
    public void to(String line, Writer writer) throws IOException {
        try{
            Gson gson = new Gson();
            Person person = gson.fromJson(line, Person.class);
            writer.write(person.toString());
        } catch (JsonSyntaxException e) {
            throw new IllegalArgumentException("Неверный JSON объект");
        }

    }
}
