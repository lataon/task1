package spring.java.lab2021.model;

import java.util.*;

public class CoinsCombination {

    public static List<List<Integer>> getCombinationsForSum(int sum, int[] arr) {
        int r = sum/Arrays.stream(arr).summaryStatistics().getMin();
        int[] copyArr = new int[arr.length+1];

        for (int i = 0; i< arr.length; i++) {
            copyArr[i] = arr[i];
        }

        return generateAll(copyArr, r, sum);
    }

    private static List<List<Integer>> generateAll(int[] arr, int r, int summary) {
        List<List<Integer>> combinations = new ArrayList<>();
        int n = arr.length;
        int[] chosen = new int[r+1];
        combinationRepetitionUtil(combinations, summary, chosen, arr, 0, r, 0, n-1);
        if (combinations.size()==0) throw new IllegalArgumentException("Неверное выражение: Нельзя разбить");
        return combinations;
    }

    private static void combinationRepetitionUtil(List<List<Integer>> list, int summaryValue, int[] chosen, int[] arr,
                                          int index, int r, int start, int end) {
        if (index == r) {
            List<Integer> data = new ArrayList<>();
            int sum = 0;
            for (int i = 0; i < r; i++) {
                int k = arr[chosen[i]];
                sum+=k;
                if (k!=0) {
                    data.add(k);
                }
            }
            if (sum ==summaryValue) {
                list.add(data);
            }
            return;
        }
        for (int i = start; i <= end; i++) {
            chosen[index] = i;
            combinationRepetitionUtil(list, summaryValue, chosen, arr, index + 1,
                    r, i, end);
        }
    }
}
