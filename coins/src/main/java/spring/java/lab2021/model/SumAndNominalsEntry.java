package spring.java.lab2021.model;

import java.util.Arrays;

public class SumAndNominalsEntry {
    private int sum;
    private int[] nominals;

    public int getSum() {
        return sum;
    }

    public int[] getNominals() {
        return nominals;
    }

    private SumAndNominalsEntry(int sum, int[] nominals) {
        this.sum = sum;
        this.nominals = nominals;
    }

    public static SumAndNominalsEntry get(String line) {
        String[] lineArguments = line.split(" ",2);
        if (lineArguments.length!=2) throw new IllegalArgumentException("Неверное выражение: Нет номиналов");
        try {
            int sum = Integer.parseInt(lineArguments[0]);
            int[] arr = Arrays.stream(lineArguments[1].split(" ")).mapToInt(Integer::parseInt).toArray();
            if (arr.length!=Arrays.stream(arr).distinct().count()) throw  new IllegalArgumentException("Неверное выражение: Номиналы повторяются");
            return new SumAndNominalsEntry(sum, arr);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Неверное выражение:  Ошибка с преобразованием в число");
        }
    }
}
