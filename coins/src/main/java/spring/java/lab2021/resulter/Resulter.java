package spring.java.lab2021.resulter;

import spring.java.lab2021.interfaces.Resultable;
import spring.java.lab2021.model.CoinsCombination;
import spring.java.lab2021.model.SumAndNominalsEntry;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.stream.Collectors;

public class Resulter implements Resultable {
    @Override
    public void to(String line, Writer writer) throws IOException {
        SumAndNominalsEntry entry = SumAndNominalsEntry.get(line);
        List<List<Integer>> coinsCombination = CoinsCombination.
                getCombinationsForSum(entry.getSum(), entry.getNominals()).
                stream().
                sorted((e, n) -> n.size()-e.size()).
                collect(Collectors.toList());
        for(List<Integer> list : coinsCombination) {
            writer.write(list.stream().map(String::valueOf).collect(Collectors.joining(" ")));
            writer.write(System.lineSeparator());
        }
    }
}
