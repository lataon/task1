package spring.java.lab2021.worker;

import org.apache.log4j.Logger;
import spring.java.lab2021.interfaces.Resultable;
import spring.java.lab2021.interfaces.Workable;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class ModuleRunner implements Workable {

    private static final Logger LOGGER = Logger.getLogger(ModuleRunner.class);

    public void doWork(String[] args, Resultable resulter) {
        final String IN_FILE_NOT_FOUND = "Отсутствует входной файл";
        final String NO_PARAMETERS = "Отсутствуют параметры";
        final String SEPARATOR = "->";

        try {
            final String FILE_IN = args[0];
            final String FILE_OUT = args[1];
            Path inPath = Paths.get(FILE_IN);

            try (BufferedWriter writer = new BufferedWriter(new FileWriter(FILE_OUT))){
                try (Scanner sc = new Scanner(inPath)) {
                    while (sc.hasNext()) {
                        String line = sc.nextLine();
                        try {
                            resulter.to(line, writer);
                            writer.newLine();
                        } catch (IllegalArgumentException e) {
                            writer.write(line + SEPARATOR + e);
                            writer.newLine();
                        }
                    }
                }  catch (IOException e) {
                    writer.write(IN_FILE_NOT_FOUND);
                }
            } catch (IOException e) {
                LOGGER.error(e);
            }

        } catch (ArrayIndexOutOfBoundsException e) {
            LOGGER.error(NO_PARAMETERS);
        }
    }
}
