package spring.java.lab2021.interfaces;

public interface Workable {
    void doWork(String[] args, Resultable resulter);
}
