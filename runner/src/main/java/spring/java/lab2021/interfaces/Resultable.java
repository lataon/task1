package spring.java.lab2021.interfaces;

import java.io.IOException;
import java.io.Writer;

public interface Resultable {
    void to(String line, Writer writer) throws IOException;
}
