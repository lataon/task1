package spring.java.lab2021.model;

import java.util.HashMap;
import java.util.Map;

public class Abbreviation {
    private static Map<String, String> abbreviations;

    static {
        abbreviations = new HashMap<>();
        abbreviations.put("АСУ","автоматизированная система управления");
        abbreviations.put("ВУЗ", "высшее учебное заведение");
        abbreviations.put("ГЭС", "гидроэлектростанция");
        abbreviations.put("ОДУ", "обыкновенное дифференциальное уравнение");
        abbreviations.put("ТАСС", "телеграфное агентство Советского Союза");
        abbreviations.put("ЯОД", "язык описания данных");
    }

    public static String decode(String line) {
        String value = abbreviations.get(line);
        if (value == null) throw new IllegalArgumentException("Неверная аббревиатура");
        return value;
    }
}
