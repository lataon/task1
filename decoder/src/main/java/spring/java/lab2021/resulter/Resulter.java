package spring.java.lab2021.resulter;

import spring.java.lab2021.interfaces.Resultable;
import spring.java.lab2021.model.Abbreviation;

import java.io.IOException;
import java.io.Writer;

public class Resulter implements Resultable {
    @Override
    public void to(String line, Writer writer) throws IOException {
        writer.write(Abbreviation.decode(line));
    }
}
