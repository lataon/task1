import spring.java.lab2021.interfaces.Resultable;
import spring.java.lab2021.interfaces.Workable;
import spring.java.lab2021.resulter.Resulter;
import spring.java.lab2021.worker.ModuleRunner;

public class Runner {

    public static void main(String[] args) {
        Workable worker = new ModuleRunner();
        Resultable resulter = new Resulter();
        worker.doWork(args, resulter);
    }
}
